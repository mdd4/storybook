# Get started
Make sure you have node.js and npm installed on your device or install it via https://nodejs.org/en/

## Install and run storybook
1. In your terminal navigate to this directory or while using visual studio code navigate to `view` > `terminal`
1. Type `npm i` in the terminal and press enter
1. Run `npm run storybook` in the terminal and press enter

# Helpful links
1. Storybook introduction: https://storybook.js.org/docs/react/get-started/introduction
1. Adding controls: https://storybook.js.org/docs/react/essentials/controls
1. Link figma with storybook: https://storybook.js.org/addons/storybook-addon-designs