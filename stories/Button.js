import './button.css';

export const createButton = ({
  primary = false,
  size = 'medium',
  backgroundColor,
  disabled = false,
  icon,
  label,
  onClick,
}) => {
  const btn = document.createElement('button');
  btn.type = 'button';
  btn.innerText = label;
  btn.disabled = disabled;
  btn.addEventListener('click', onClick);

  if(icon) {
    const img = document.createElement('img')
    img.src = icon
    img.className = 'storybook-button__icon'
    btn.prepend(img)
  }

  console.log(icon)

  const mode = primary ? 'storybook-button--primary' : 'storybook-button--secondary';
  const canClick = disabled ? 'storybook-button--disabled' : ''
  btn.className = ['storybook-button', `storybook-button--${size}`, mode, canClick].join(' ');

  btn.style.backgroundColor = backgroundColor;

  return btn;
};
