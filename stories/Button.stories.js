import { createButton } from './Button';
import { withDesign } from 'storybook-addon-designs'
import icon from './assets/ICN_placeholder.png'

export default {
  title: 'Example/Button',
  argTypes: {
    label: { control: 'text' },
    primary: { control: 'boolean' },
    disabled: { control: 'boolean'},
    backgroundColor: { control: 'color' },
    icon: {control: 'file'},
    size: {
      control: { type: 'select' },
      options: ['small', 'medium', 'large']
    },
    onClick: { action: 'onClick' },
  },
  decorators: [withDesign],
};

const Template = ({ label, ...args }) => {
  // You can either use a function to create DOM elements or use a plain html string!
  // return `<div>${label}</div>`;
  return createButton({ label, ...args });
};

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Button',
  icon: ''
};
Primary.parameters = {
  design: {
    type: 'figma',
    url: 'https://www.figma.com/file/wsBncy3j0IHTRMDJapNSPc/MDD?node-id=118%3A44',
  },
}

export const Secondary = Template.bind({});
Secondary.args = {
  primary: false,
  label: 'Button',
  icon: ''
};
Secondary.parameters = {
  design: {
    type: 'figma',
    url: 'https://www.figma.com/file/wsBncy3j0IHTRMDJapNSPc/MDD?node-id=129%3A45',
  },
}

export const Large = Template.bind({});
Large.args = {
  primary: false,
  size: 'large',
  label: 'Button',
  icon: ''
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Button',
  icon: ''
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  label: 'Button',
  icon: icon
};

export const Disabled = Template.bind({});
Disabled.args = {
  label: 'Button',
  icon: '',
  disabled: true
}
